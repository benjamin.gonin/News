package com.gonin.news

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
@Suppress("IllegalIdentifier")
class ExampleUnitTest {

    @Test
    @Throws(Exception::class)
    fun `addition is correct`() {
        assertEquals(4, (2 + 2).toLong())
    }

}